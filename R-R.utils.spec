%global packname R.utils
%global packver  2.10.1
%global rlibdir  %{_datadir}/R/library

Name:             R-%{packname}
Version:          2.10.1
Release:          1
Summary:          Various Programming Utilities

License:          LGPLv2+
URL:              https://CRAN.R-project.org/package=%{packname}
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{packver}.tar.gz

BuildArch:        noarch
BuildRequires:    R-devel
BuildRequires:    tex(latex)
BuildRequires:    R-R.oo >= 1.23.0
BuildRequires:    R-methods
BuildRequires:    R-utils
BuildRequires:    R-tools
BuildRequires:    R-R.methodsS3 >= 1.8.0
BuildRequires:    R-digest >= 0.6.10

%description
Utility functions useful when programming and developing R packages.


%prep
%setup -q -c -n %{packname}


%build


%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css


%check
%{_bindir}/R CMD check %{packname}


%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/DESCRIPTION
%doc %{rlibdir}/%{packname}/NEWS
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help
%{rlibdir}/%{packname}/WORDLIST
%{rlibdir}/%{packname}/data-ex


%changelog
* Thu Jun 16 2022 misaka00251 <misaka00251@misakanet.cn> - 2.10.1-1
- Init package (Thanks to fedora team)
